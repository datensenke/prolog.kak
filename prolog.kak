# Kakoune syntax file
# Language:    PROLOG
# Maintainer:  Dinis de Brito e Cunha<dinis@chello.at>
# Last Change: 16.5.2020
#
# Detection
# ‾‾‾‾‾‾‾‾‾

hook global BufCreate .*\.(pro|pl) %{
    set-option buffer filetype prolog
}

# Highlighters
# ‾‾‾‾‾‾‾‾‾‾‾‾

add-highlighter shared/prolog regions
add-highlighter shared/prolog/line_comment region '%' '$' fill comment
add-highlighter shared/prolog/multi_line_comment region '/\*' '\*/' fill comment

add-highlighter shared/prolog/double_string region %{(?<!')"} %{(?<!\\)(\\\\)*"} group
add-highlighter shared/prolog/double_string/ fill string
add-highlighter shared/prolog/double_string/ regex ~([a-z]) 0:+b@string

add-highlighter shared/prolog/single_string region %{(?<!")'} %{(?<!\\)(\\\\)*'} group
add-highlighter shared/prolog/single_string/ fill string
add-highlighter shared/prolog/single_string/ regex ~([a-z]) 0:+b@string

# list
add-highlighter shared/prolog/list region -recurse \[ '\[' '\]' regions
add-highlighter shared/prolog/list/content default-region group
add-highlighter shared/prolog/list/content/ ref prolog/body
add-highlighter shared/prolog/list/content/ regex [\[\]\|] 0:+b@variable

add-highlighter shared/prolog/list/d_s region %{(?<!')"} %{(?<!\\)(\\\\)*"} group
add-highlighter shared/prolog/list/d_s/ fill string
add-highlighter shared/prolog/list/d_s/ regex ~([a-z]) 0:+b@string

add-highlighter shared/prolog/list/i_s region %{(?<!")'} %{(?<!\\)(\\\\)*'} group
add-highlighter shared/prolog/list/i_s/ fill string
add-highlighter shared/prolog/list/i_s/ regex ~([a-z]) 0:+b@string

add-highlighter shared/prolog/list/multi_line_comment region '/\*' '\*/' fill comment
# head
add-highlighter shared/prolog/head region '^[a-z_]' '(:-|-->|\.)' regions
add-highlighter shared/prolog/head/content default-region group
add-highlighter shared/prolog/head/content/ fill function
add-highlighter shared/prolog/head/content/ regex ':-|-->' 0:operator
add-highlighter shared/prolog/head/content/ regex \(\h*([^\h,\)]+)|,\h*([^\h,\)]+) 1:type 2:type
add-highlighter shared/prolog/head/content/ regex \(\h*([a-z_]+)|,\h*([a-z_]+) 1:+b@type 2:+b@type

add-highlighter shared/prolog/head/list region -recurse \[ '\[' '\]' ref prolog/list

add-highlighter shared/prolog/head/double_string region %{(?<!')"} %{(?<!\\)(\\\\)*"} group
add-highlighter shared/prolog/head/double_string/ fill string
add-highlighter shared/prolog/head/double_string/ regex ~([a-z]) 0:+b@string

add-highlighter shared/prolog/head/single_string region %{(?<!")'} %{(?<!\\)(\\\\)*'} group
add-highlighter shared/prolog/head/single_string/ fill string
add-highlighter shared/prolog/head/single_string/ regex ~([a-z]) 0:+b@string
# body
#add-highlighter shared/prolog/body region '(?<=:-)' '.\h*$' regions
add-highlighter shared/prolog/body default-region regions
add-highlighter shared/prolog/body/content default-region group

#add-highlighter shared/prolog/body/ regex ^[a-z][A-Za-z0-9_]*.*(?=(:-|-->|\.)) 0:function
#    Integer format's
add-highlighter shared/prolog/body/content/ regex '(?i)\b0b[01]+l?\b' 0:value
add-highlighter shared/prolog/body/content/ regex '(?i)\b0x[\da-f]+l?\b' 0:value
add-highlighter shared/prolog/body/content/ regex '(?i)\b0o?[0-7]+l?\b' 0:value
add-highlighter shared/prolog/body/content/ regex '(?i)\b([1-9]\d*|0)l?\b' 0:value
#    Float formats
add-highlighter shared/prolog/body/content/ regex '\b\d+[eE][+-]?\d+\b' 0:value
add-highlighter shared/prolog/body/content/ regex '(\b\d+)?\.\d+\b' 0:value
add-highlighter shared/prolog/body/content/ regex '\b\d+\.' 0:value
#
add-highlighter shared/prolog/body/content/ regex (#?[=\\=\|=:=\|\\==\|=<\|==\|>=\|\\=\|\\+\|<\|>\|\-\|@>\|@<\|]) 0:operator
add-highlighter shared/prolog/body/content/ regex (!) 0:+u@operator
add-highlighter shared/prolog/body/content/ regex '\b(([A-Z_][A-Za-z0-9_]*\h*(?!\())|([a-z_]+))\b\h*(?!\()' 2:variable 3:+b@variable

#keywords
evaluate-commands %sh{
    # Grammar
    keywords="abolish|current_output|peek_code|label|labeling"
    keywords="${keywords}|append|current_predicate|put_byte"
    keywords="${keywords}|arg|current_prolog_flag|put_char"
    keywords="${keywords}|asserta|fail|put_code"
    keywords="${keywords}|assertz|findall|read"
    keywords="${keywords}|at_end_of_stream|float|read_term"
    keywords="${keywords}|atom|flush_output|repeat"
    keywords="${keywords}|atom_chars|functor|retract"
    keywords="${keywords}|atom_codes|get_byte|set_input"
    keywords="${keywords}|atom_concat|get_char|set_output"
    keywords="${keywords}|atom_length|get_code|set_prolog_flag"
    keywords="${keywords}|atomic|halt|set_stream_position"
    keywords="${keywords}|bagof|integer|setof"
    keywords="${keywords}|call|is|stream_property"
    keywords="${keywords}|catch|nl|sub_atom"
    keywords="${keywords}|char_code|nonvar|throw"
    keywords="${keywords}|char_conversion|number|true"
    keywords="${keywords}|clause|number_chars|unify_with_occurs_check"
    keywords="${keywords}|close|number_codes|var"
    keywords="${keywords}|compound|once|write"
    keywords="${keywords}|copy_term|op|write_canonical"
    keywords="${keywords}|current_char_conversion|open|write_term"
    keywords="${keywords}|current_input|peek_byte|writeq"
    keywords="${keywords}|current_op|peek_char"
    keywords="${keywords}|use_module|module"

    # Add the language's grammar to the static completion list
    printf %s\\n "hook global WinSetOption filetype=prolog %{
        set-option window static_words '${keywords}'
    }" | sed 's,|,:,g'

    # Highlight keywords
 printf %s "add-highlighter shared/prolog/body/content/ regex \b(${keywords})\b 0:keyword"
}

hook global WinSetOption filetype=prolog %{
    hook window InsertChar \n -group prolog-indent prolog-indent-on-new-line
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window prolog.+ }
}
hook -group prolog-highlight global WinSetOption filetype=prolog %{
    add-highlighter window/prolog ref prolog 
}

hook -group prolog-highlight global WinSetOption filetype=(?!prolog).* %{
    remove-highlighter window/prolog
}


# Commands
# ‾‾‾‾‾‾‾‾


define-command -hidden prolog-indent-on-new-line %{
    evaluate-commands -draft -itersel %{
        # copy '%' comment prefix and following white spaces
        try %{ execute-keys -draft k <a-x> s ^\h*\K%\h* <ret> y gh j P }
        # preserve previous line indent
        try %{ execute-keys -draft <semicolon> K <a-&> }
        # cleanup trailing whitespaces from previous line
        try %{ execute-keys -draft k <a-x> s \h+$ <ret> d }
        # indent after line ending with :- or -->
        try %{ execute-keys -draft k <a-x> <a-k> (:-|-->)$ <ret> j <a-gt> }
        # un-indent after line ending with .
        try %{ execute-keys -draft k <a-x> <a-k> \.$ <ret> j <a-lt> }
    }
}
